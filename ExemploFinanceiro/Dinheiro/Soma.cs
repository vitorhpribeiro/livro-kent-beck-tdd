﻿using System.Linq.Expressions;

namespace Dinheiro.Testes
{
    public class Soma : Expressao
    {
        public Expressao _primeiroTermo;
        public Expressao _segundoTermo;

        public Soma(Expressao primeiroTermo, Expressao segundoTermo)
        {
            this._primeiroTermo = primeiroTermo;
            this._segundoTermo = segundoTermo;
        }

        public Dinheiro Reduzir(Banco banco, string para)
        {
            double montante = _primeiroTermo.Reduzir(banco, para)._montante +
                              _segundoTermo.Reduzir(banco, para)._montante;
            return new Dinheiro(montante, para);
        }

        public Expressao Somar(Expressao segundoTermo)
        {
            return new Soma(this, segundoTermo);
        }

        public Expressao Vezes(double multiplicador)
        {
            return new Soma(_primeiroTermo.Vezes(multiplicador), _segundoTermo.Vezes(multiplicador));
        }
    }
}
