﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dinheiro.Testes;

namespace Dinheiro
{
    public interface Expressao
    {
        Dinheiro Reduzir(Banco banco, string to);
        Expressao Somar(Expressao segundoTermo);
        Expressao Vezes(double multiplicador);
    }
}
