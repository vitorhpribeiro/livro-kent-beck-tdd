﻿using System;
using System.Collections;
using System.Linq.Expressions;


namespace Dinheiro.Testes
{
    public class Banco
    {
        private Hashtable taxas = new Hashtable();

        public Dinheiro Reduzir(Expressao source, string para)
        {
            return source.Reduzir(this, para);
        }

        public double Taxa(string de, string para)
        {
            return (de.Equals("CHF") && para.Equals("USD")) ? 2 : 1;
        }

        public void AdicionaTaxa(string de, string para, double taxa)
        {
            taxas.Add(new Par(de, para), taxa);
        }
    }
}
