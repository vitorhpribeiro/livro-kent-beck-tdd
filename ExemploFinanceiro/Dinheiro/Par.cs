﻿using System;

namespace Dinheiro
{
    internal class Par
    {
        private string _de;
        private string _para;

        public Par(string de, string para)
        {
            this._de = de;
            this._para = para;
        }

        public bool Equals(Object objeto)
        {
            Par par = (Par) objeto;
            return _de.Equals(par._de) && _para.Equals(par._para);
        }

        public int HashCode()
        {
            return 0;
        }
    }
}
