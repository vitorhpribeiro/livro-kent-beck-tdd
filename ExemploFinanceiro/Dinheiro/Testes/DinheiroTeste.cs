﻿using System.Collections;
using System.ComponentModel;
using System.Linq.Expressions;
using NUnit.Framework;


namespace Dinheiro.Testes
{
    [TestFixture]
    class DinheiroTeste
    {
        [Test]
        public void TesteDeMultiplicacaoDeDolares()
        {
            Dinheiro cinco = Dinheiro.Doletas(5, "USD");
            Assert.AreEqual(Dinheiro.Doletas(10, "USD"), cinco.Vezes(2));
            Assert.AreEqual(Dinheiro.Doletas(15, "USD"), cinco.Vezes(3));
        }
        
        [Test]
        public void TesteDeMultiplicacaoDeFranc()
        {
            Dinheiro cinco = Dinheiro.Fracs(5, "CHF");
            var montanteEsperado = Dinheiro.Fracs(10, "CHF");
            var montanteObtido = cinco.Vezes(2);

            Assert.AreEqual(montanteEsperado, montanteObtido);
        }

        [Test]
        public void TesteDeIgualdade()
        {
            Assert.True(Dinheiro.Doletas(5, "USD").Equals(Dinheiro.Doletas(5, "USD")));
            Assert.False(new Dolar(5, "USD").Equals(new Dolar(6, "USD")));
        }

        [Test]
        public void TesteDeTipoDeMoeda()
        {
            Assert.AreEqual("USD", Dinheiro.Doletas(5, "USD").TipoDeMoeda());
            Assert.AreEqual("CHF", Dinheiro.Fracs(5, "CHF").TipoDeMoeda());
        }

        [Test]
        public void TesteDeIgualdadeDeClassesDiferentes()
        {
            Assert.False(new Dolar(10, "USD").Equals(new Franc(10, "CHF")));
            Assert.True(new Dinheiro(10, "CHF").Equals(new Franc(10, "CHF")));
        }

        [Test]
        public void TesteDeUmaSimplesSoma()
        {
            Dinheiro cinco = Dinheiro.Doletas(5, "USD");
            Expressao soma = cinco.Somar(cinco);
            Banco banco = new Banco();
            Dinheiro reducao = banco.Reduzir(soma, "USD");
            Assert.AreEqual(Dinheiro.Doletas(10, "USD"), reducao);
        }

        [Test]
        public void TesteSomaRetornaUmaSoma()
        {
            Dinheiro cinco = Dinheiro.Doletas(5, "USD");
            Expressao resultado = cinco.Somar(cinco);
            Soma soma = (Soma) resultado;
            Assert.AreEqual(cinco, soma._primeiroTermo);
            Assert.AreEqual(cinco, soma._segundoTermo);
        }

        [Test]
        public void TesteReduzirSoma()
        {
            Expressao soma = new Soma(Dinheiro.Doletas(3, "USD"), Dinheiro.Doletas(4, "USD"));
            Banco banco = new Banco();
            Dinheiro resultado = banco.Reduzir(soma, "USD");
            Assert.AreEqual(Dinheiro.Doletas(7, "USD"), resultado);
        }

        [Test]
        public void TesteReduzirDinheiro()
        {
            Banco banco = new Banco();
            Dinheiro resultado = banco.Reduzir(Dinheiro.Doletas(1, "USD"), "USD");
            Assert.AreEqual(Dinheiro.Doletas(1, "USD"), resultado);
        }

        [Test]
        public void TesteReduzirDiferencaDeDinheiroEntreMoedasDiferentes()
        {
            Banco banco = new Banco();
            banco.AdicionaTaxa("CHF", "USD", 2);
            Dinheiro resultado = banco.Reduzir(Dinheiro.Fracs(2, "CHF"), "USD");
            Assert.AreEqual(Dinheiro.Doletas(1, "USD"), resultado);
        }

        [Test]
        public void TesteArraysIguais()
        {
            Assert.AreEqual(new object[] {"abc"}, new object [] {"abc"});
        }

        [Test]
        public void TesteDeSomaDeVariosTiposDeMoedas()
        {
            Expressao cincoDoletas = Dinheiro.Doletas(5, "USD");
            Expressao dezFracs = Dinheiro.Fracs(10, "CHF");
            Banco banco = new Banco();
            banco.AdicionaTaxa("CHF", "USD", 2);
            Dinheiro resultado = banco.Reduzir(cincoDoletas.Somar(dezFracs), "USD");
            Assert.AreEqual(Dinheiro.Doletas(10, "USD"), resultado);
        }

        [Test]
        public void TesteSomarDinheiroComSoma()
        {
            Expressao cincoDoletas = Dinheiro.Doletas(5, "USD");
            Expressao dezFracs = Dinheiro.Fracs(10, "CHF");
            Banco banco = new Banco();
            banco.AdicionaTaxa("CHF", "USD", 2);
            Expressao soma = new Soma(cincoDoletas, dezFracs).Somar(cincoDoletas);
            Dinheiro resultado = banco.Reduzir(soma, "USD");
            Assert.AreEqual(Dinheiro.Doletas(15, "USD"), resultado);
        }

        [Test]
        public void TesteDeMultiplicacao()
        {
            Expressao cincoDoletas = Dinheiro.Doletas(5, "USD");
            Expressao dezFracs = Dinheiro.Fracs(10, "CHF");
            Banco banco = new Banco();
            banco.AdicionaTaxa("CHF", "USD", 2);
            Expressao soma = new Soma(cincoDoletas, dezFracs).Vezes(2);
            Dinheiro resultado = banco.Reduzir(soma, "USD");
            Assert.AreEqual(Dinheiro.Doletas(20, "USD"), resultado);
        }
    }
}
