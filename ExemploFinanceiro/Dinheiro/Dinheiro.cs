﻿using Dinheiro.Testes;
using System;
using System.Linq.Expressions;

namespace Dinheiro
{
    public class Dinheiro : Expressao
    {
        public double _montante;
        protected string _tipoDeMoeda;

        public Dinheiro(double montante, string tipoDeMoeda)
        {
            this._montante = montante;
            this._tipoDeMoeda = tipoDeMoeda;
        }

        public static explicit operator Dinheiro(bool v)
        {
            throw new NotImplementedException();
        }

        public override bool Equals(Object objeto)
        {
            Dinheiro dinheiro = (Dinheiro)objeto;
            return _montante == dinheiro._montante && _tipoDeMoeda.Equals(dinheiro._tipoDeMoeda);
        }

        public static Dinheiro Doletas(double montante, string tipoDeMoeda)
        {
            return new Dolar(montante, "USD");
        }

        public static Dinheiro Fracs(double montante, string tipoDeMoeda)
        {
            return new Franc(montante, "CHF");
        }

        public Expressao Vezes(double multiplicador)
        {
            return new Dinheiro(_montante * multiplicador, _tipoDeMoeda);
        }

        public virtual string TipoDeMoeda()
        {
            return _tipoDeMoeda;
        }

        public Expressao Somar(Expressao dinheiroQueSeraSomado)
        {
            return new Soma(this, dinheiroQueSeraSomado);
        }

        public Dinheiro Reduzir(Banco banco, string para)
        {
            double taxa = banco.Taxa(_tipoDeMoeda, para);
            return new Dinheiro(_montante / taxa, para);
        }
    }
}
